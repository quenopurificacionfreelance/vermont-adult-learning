<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Val
 */

get_header(); ?>

    <header class="single-page-header bg-green">
        <div class="container clearfix">
            <div class="grid_12 omega">
                <h1>News</h1>
            </div>
        </div>
    </header>

    <div class="container clearfix">
        <div class="grid_8">
            <main id="main" class="site-main">

                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', get_post_type() );

                    //the_post_navigation();

                    // If comments are open or we have at least one comment, load up the comment template.
                    // if ( comments_open() || get_comments_number() ) :
                    //     comments_template();
                    // endif;

                endwhile; // End of the loop.
                ?>

            </main><!-- #main -->
        </div><!-- #primary -->

        <?php get_sidebar(); ?>

    </div>

<?php
get_footer();
