<?php 
$background_image = get_sub_field('background_image');            
$grid_size = get_sub_field('grid_size');            
$content = get_sub_field('content');
$content_position = get_sub_field('content_position');

?>
<section class="content-grid" style="background-image: url(<?php echo $background_image['url']; ?>);">
    <div class="spacer">
        <div class="container">
            <div class="<?php echo $grid_size.' '.$content_position; ?> omega">
                <?php echo $content; ?>
            </div>
            <div class="clearfix"></div>
        </div>            
    </div>        
</section>