<section class="content-repeater">
	<div class="spacer">
		<div class="container">
			<?php 
			if( have_rows('add_content') ):
				$posts_count = 1;
				while( have_rows('add_content') ): the_row();
					$main_content = get_sub_field('main_content');
					$grid = get_sub_field('grid');
					?>					
					<div class="<?php echo $grid; ?> omega">
						<div class="content content_<?php echo $grid; ?>">
							<?php echo $main_content; ?>
						</div>
					</div>

					<?php
					if($grid == 'grid_6') {								
						if ($posts_count % 2 == 0):
					        echo '<div class="clearfix"></div>';					        
					    else:
					        echo ' ';
					    endif;
					    $posts_count++;
					}
				endwhile;
			endif;
			?>
		<div class="clearfix"></div>
		</div>
	</div>
</section>