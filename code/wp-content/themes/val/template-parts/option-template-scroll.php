<?php 

$banner_image = get_field('banner_image', 'option');
$background_color = get_field('background_color', 'option');
$banner_content = get_field('banner_content_image', 'option'); 
$link_label = get_field('link_label', 'option');
?>

<section class="scroll_banner" style="background-image: url(<?php echo $banner_image['url'];?>);">
	<div class="masking" style="background-color: <?php echo $background_color;?>;">
		<div class="container">
			<div class="grid_12 omega">
				<div class="spacer">
					<img src="<?php echo $banner_content['url'];?>" alt="<?php echo $banner_content['alt'];?>" class="img-responsive">
					<p><a href="#main" class="btn-curve-white"><span></span><?php echo $link_label; ?></a></p>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</section>
<section id="main"></section>
