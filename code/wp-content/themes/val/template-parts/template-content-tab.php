<section class="content-tab">
	<div class="spacer">
		<div class="container">
			<div class="grid_12 omega">
				<ul id="mytabs">				
					<?php 			
					$tabNumber = 1;
					
					while( have_rows('tab_content') ): the_row();
						if($tabNumber == 1) {
							$tabActive = 'active';
						} else {
							$tabActive = '';
						}
						$title = get_sub_field('title');
						$content = get_sub_field('content');

						echo '<li id="tab'.$tabNumber.'" class="'.$tabActive.' tabmenu">'.$title.'</li>';		
						$tabNumber++;			
					endwhile;
					?>
				</ul>
				<div class="content">				
					<?php
					$tabContent = 1;				
					while( have_rows('tab_content') ): the_row();
						if($tabContent == 1) {
							$tabSelected = 'selected';
						} else {
							$tabSelected = '';
						}
						$title = get_sub_field('title');
						$content = get_sub_field('content'); ?>
						<div id="tab<?php echo $tabContent; ?>_content" class="<?php echo $tabSelected; ?>">
							<?php echo $content; ?>
						</div>
						<?php
						$tabContent++;
					endwhile;
					?>
					
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<script type="text/javascript">
		( function( $ ) {
	      'use strict';
	      $('.content-tab ul#mytabs li.tabmenu').click(function () {
	        $(this).addClass('active').siblings().removeClass('active');
	        
	        $('.content-tab .content div').not('#' + $(this).attr('id') + '_content').removeClass('selected').fadeOut();
	        $('#' + $(this).attr('id') + '_content').addClass('selected').fadeIn();
	        console.log('Tab');
	      });
	    } )( jQuery );
	</script>
</section>