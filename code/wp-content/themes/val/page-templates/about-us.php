<?php
/*
 * Template name: About us
 */

get_header(); ?>
<section>
    <?php get_template_part( 'template-parts/header', 'content' ); ?>
</section> 
<div class="container">
    <div class="row">

        <div id="primary" class="content-area col-lg-8">
            <main id="main" class="site-main">

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'template-parts/content', 'page' );?>
                    
                <?php endwhile ;?>
            </main>
        </div>

    </div>
</div>

<section>
    <div class="profile-info-container" style="background-image: url(<?php the_field('image_background');?>);">
        <div class="container clearfix">
        
            <div class="grid_4">
                <?php $section = get_field('profile'); if( $section ): ?>
                    <br>
                    <img src="<?php echo $section['profile_photo'];?>">
                    <h2><?php echo $section['name'];?></h2>
                    <h5><?php echo $section['title'];?></h5>
                <?php endif; ?>
            </div>
            <div class="grid_8 omega">
                <?php $section = get_field('profile_description'); if( $section ): ?>
                    <?php echo $section['information'];?>
                <?php endif;?>
            </div> 
        
        </div>
    </div>
</section>
<?php
get_footer();?>