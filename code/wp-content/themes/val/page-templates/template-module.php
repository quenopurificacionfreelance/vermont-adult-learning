<?php
/*
 * Template name: Module Template
 */
get_header(); ?>
    
<?php

// check if the flexible content field has rows of data
if( have_rows('module') ):

     // loop through the rows of data
    $counter = 1;
    while ( have_rows('module') ) : the_row();

        if( get_row_layout() == 'carousel_banner' ): 

            if( have_rows('carousel') ): 
                include( locate_template( 'template-parts/template-carousel.php', false, false ) );
                // get_template_part( 'template-parts/template', 'carousel' ); 
            endif;

        elseif( get_row_layout() == 'content' ):

            include( locate_template( 'template-parts/template-content.php', false, false ) );

        elseif(get_row_layout() == 'content_grid'):
            
<<<<<<< HEAD
            include( locate_template( 'template-parts/template-content-grid.php', false, false ) );

        elseif(get_row_layout() == 'content_repeater'):

            include( locate_template( 'template-parts/template-content-repeater.php', false, false ) );
=======
            $background_image = get_sub_field('background_image');            
            $grid_size = get_sub_field('grid_size');            
            $content = get_sub_field('content');
            $content_position = get_sub_field('content_position');
            
            ?>
                <section class="content-grid" style="background-image: url(<?php echo $background_image['url']; ?>);">
                    <div class="spacer">
                        <div class="container">
                            <div class="<?php echo $grid_size.' '.$content_position; ?> omega">
                                <?php echo $content; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                    </div>
                    
                </section>

            <?php 
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

        elseif( get_row_layout() == 'content_with_thumbnails' ):
                    
            $content = get_sub_field('content');
            if( have_rows('thumbnails') ):
                include( locate_template( 'template-parts/template-content-thumbnails.php', false, false ) );
            endif;

        elseif(get_row_layout() == 'flexible_image_with_single_content'):

            $label = get_sub_field('label');            
            if( have_rows('content_repeater') ):
                include( locate_template( 'template-parts/template-image-position.php', false, false ) );                
            endif;
<<<<<<< HEAD

        elseif(get_row_layout() == 'content_tab'):

            if( have_rows('tab_content') ):
                include( locate_template( 'template-parts/template-content-tab.php', false, false ) );                
            endif;

        endif;
        $counter++;
=======
            
        endif;
       $counter++;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
    endwhile;

else :

    // no layouts found

endif;

?>    
    
<?php get_footer();?>


