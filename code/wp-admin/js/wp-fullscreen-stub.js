/**
 * Distraction-Free Writing (wp-fullscreen) backward compatibility stub.
<<<<<<< HEAD
 *
 * @deprecated 4.1
 * @removed 4.3.
 * @output wp-admin/js/wp-fullscreen-stub.js
=======
 * Todo: remove at the end of 2016.
 *
 * Original was deprecated in 4.1, removed in 4.3.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
 */
( function() {
	var noop = function(){};

	window.wp = window.wp || {};
	window.wp.editor = window.wp.editor || {};
	window.wp.editor.fullscreen = {
		bind_resize: noop,
		dfwWidth: noop,
		off: noop,
		on: noop,
		refreshButtons: noop,
		resizeTextarea: noop,
		save: noop,
		switchmode: noop,
		toggleUI: noop,

		settings: {},
		pubsub: {
			publish: noop,
			subscribe: noop,
			unsubscribe: noop,
			topics: {}
		},
		fade: {
			In: noop,
			Out: noop
		},
		ui: {
			fade: noop,
			init: noop
		}
	};
}());
