<<<<<<< HEAD
/**
 * @output wp-admin/js/language-chooser.js
 */

jQuery( function($) {
/*
 * Set the correct translation to the continue button and show a spinner
 * when downloading a language.
 */
=======
jQuery( function($) {

>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
var select = $( '#language' ),
	submit = $( '#language-continue' );

if ( ! $( 'body' ).hasClass( 'language-chooser' ) ) {
	return;
}

select.focus().on( 'change', function() {
<<<<<<< HEAD
	/*
	 * When a language is selected, set matching translation to continue button
	 * and attach the language attribute.
	 */
=======
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	var option = select.children( 'option:selected' );
	submit.attr({
		value: option.data( 'continue' ),
		lang: option.attr( 'lang' )
	});
});

$( 'form' ).submit( function() {
<<<<<<< HEAD
	// Show spinner for languages that need to be downloaded.
=======
	// Don't show a spinner for English and installed languages,
	// as there is nothing to download.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	if ( ! select.children( 'option:selected' ).data( 'installed' ) ) {
		$( this ).find( '.step .spinner' ).css( 'visibility', 'visible' );
	}
});

});
