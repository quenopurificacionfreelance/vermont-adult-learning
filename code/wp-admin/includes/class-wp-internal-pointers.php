<?php
/**
 * Administration API: WP_Internal_Pointers class
 *
 * @package WordPress
 * @subpackage Administration
 * @since 4.4.0
 */

/**
 * Core class used to implement an internal admin pointers API.
 *
 * @since 3.3.0
 */
final class WP_Internal_Pointers {
	/**
	 * Initializes the new feature pointers.
	 *
	 * @since 3.3.0
	 *
	 * All pointers can be disabled using the following:
	 *     remove_action( 'admin_enqueue_scripts', array( 'WP_Internal_Pointers', 'enqueue_scripts' ) );
	 *
	 * Individual pointers (e.g. wp390_widgets) can be disabled using the following:
	 *     remove_action( 'admin_print_footer_scripts', array( 'WP_Internal_Pointers', 'pointer_wp390_widgets' ) );
	 *
<<<<<<< HEAD
=======
	 * @static
	 *
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 * @param string $hook_suffix The current admin page.
	 */
	public static function enqueue_scripts( $hook_suffix ) {
		/*
		 * Register feature pointers
		 *
		 * Format:
		 *     array(
		 *         hook_suffix => pointer callback
		 *     )
		 *
		 * Example:
		 *     array(
		 *         'themes.php' => 'wp390_widgets'
		 *     )
		 */
		$registered_pointers = array(
<<<<<<< HEAD
			//None currently.
		);

		// Check if screen related pointer is registered
		if ( empty( $registered_pointers[ $hook_suffix ] ) ) {
			return;
		}
=======
			'index.php' => 'wp496_privacy',
		);

		// Check if screen related pointer is registered
		if ( empty( $registered_pointers[ $hook_suffix ] ) )
			return;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		$pointers = (array) $registered_pointers[ $hook_suffix ];

		/*
		 * Specify required capabilities for feature pointers
		 *
		 * Format:
		 *     array(
		 *         pointer callback => Array of required capabilities
		 *     )
		 *
		 * Example:
		 *     array(
		 *         'wp390_widgets' => array( 'edit_theme_options' )
		 *     )
		 */
		$caps_required = array(
<<<<<<< HEAD
			// None currently.
=======
			'wp496_privacy' => array(
				'manage_privacy_options',
				'export_others_personal_data',
				'erase_others_personal_data',
			),
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		);

		// Get dismissed pointers
		$dismissed = explode( ',', (string) get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true ) );

		$got_pointers = false;
		foreach ( array_diff( $pointers, $dismissed ) as $pointer ) {
			if ( isset( $caps_required[ $pointer ] ) ) {
				foreach ( $caps_required[ $pointer ] as $cap ) {
<<<<<<< HEAD
					if ( ! current_user_can( $cap ) ) {
						continue 2;
					}
=======
					if ( ! current_user_can( $cap ) )
						continue 2;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
				}
			}

			// Bind pointer print function
			add_action( 'admin_print_footer_scripts', array( 'WP_Internal_Pointers', 'pointer_' . $pointer ) );
			$got_pointers = true;
		}

<<<<<<< HEAD
		if ( ! $got_pointers ) {
			return;
		}
=======
		if ( ! $got_pointers )
			return;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		// Add pointers script and style to queue
		wp_enqueue_style( 'wp-pointer' );
		wp_enqueue_script( 'wp-pointer' );
	}

	/**
	 * Print the pointer JavaScript data.
	 *
	 * @since 3.3.0
	 *
<<<<<<< HEAD
=======
	 * @static
	 *
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 * @param string $pointer_id The pointer ID.
	 * @param string $selector The HTML elements, on which the pointer should be attached.
	 * @param array  $args Arguments to be passed to the pointer JS (see wp-pointer.js).
	 */
	private static function print_js( $pointer_id, $selector, $args ) {
<<<<<<< HEAD
		if ( empty( $pointer_id ) || empty( $selector ) || empty( $args ) || empty( $args['content'] ) ) {
			return;
		}
=======
		if ( empty( $pointer_id ) || empty( $selector ) || empty( $args ) || empty( $args['content'] ) )
			return;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		?>
		<script type="text/javascript">
		(function($){
			var options = <?php echo wp_json_encode( $args ); ?>, setup;

			if ( ! options )
				return;

			options = $.extend( options, {
				close: function() {
					$.post( ajaxurl, {
						pointer: '<?php echo $pointer_id; ?>',
						action: 'dismiss-wp-pointer'
					});
				}
			});

			setup = function() {
				$('<?php echo $selector; ?>').first().pointer( options ).pointer('open');
			};

			if ( options.position && options.position.defer_loading )
				$(window).bind( 'load.wp-pointers', setup );
			else
				$(document).ready( setup );

		})( jQuery );
		</script>
		<?php
	}

	public static function pointer_wp330_toolbar() {}
	public static function pointer_wp330_media_uploader() {}
	public static function pointer_wp330_saving_widgets() {}
	public static function pointer_wp340_customize_current_theme_link() {}
	public static function pointer_wp340_choose_image_from_library() {}
	public static function pointer_wp350_media() {}
	public static function pointer_wp360_revisions() {}
	public static function pointer_wp360_locks() {}
	public static function pointer_wp390_widgets() {}
	public static function pointer_wp410_dfw() {}
<<<<<<< HEAD
	public static function pointer_wp496_privacy() {}
=======

	/**
	 * Display a pointer for the new privacy tools.
	 *
	 * @since 4.9.6
	 */
	public static function pointer_wp496_privacy() {
		$content  = '<h3>' . __( 'Personal Data and Privacy' ) . '</h3>';
		$content .= '<h4>' . __( 'Personal Data Export and Erasure' ) . '</h4>';
		$content .= '<p>' . __( 'New <strong>Tools</strong> have been added to help you with personal data export and erasure requests.' ) . '</p>';
		$content .= '<h4>' . __( 'Privacy Policy' ) . '</h4>';
		$content .= '<p>' . __( 'Create or select your site&#8217;s privacy policy page under <strong>Settings &gt; Privacy</strong> to keep your users informed and aware.' ) . '</p>';

		if ( is_rtl() ) {
			$position = array(
				'edge'  => 'right',
				'align' => 'bottom',
			);
		} else {
			$position = array(
				'edge'  => 'left',
				'align' => 'bottom',
			);
		}

		$js_args = array(
			'content'  => $content,
			'position' => $position,
			'pointerClass' => 'wp-pointer arrow-bottom',
			'pointerWidth' => 420,
		);
		self::print_js( 'wp496_privacy', '#menu-tools', $js_args );
	}
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

	/**
	 * Prevents new users from seeing existing 'new feature' pointers.
	 *
	 * @since 3.3.0
	 *
<<<<<<< HEAD
	 * @param int $user_id User ID.
	 */
	public static function dismiss_pointers_for_new_users( $user_id ) {
		add_user_meta( $user_id, 'dismissed_wp_pointers', '' );
=======
	 * @static
	 *
	 * @param int $user_id User ID.
	 */
	public static function dismiss_pointers_for_new_users( $user_id ) {
		add_user_meta( $user_id, 'dismissed_wp_pointers', 'wp496_privacy' );
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	}
}
