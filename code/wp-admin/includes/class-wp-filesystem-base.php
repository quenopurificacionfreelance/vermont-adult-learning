<?php
/**
 * Base WordPress Filesystem
 *
 * @package WordPress
 * @subpackage Filesystem
 */

/**
<<<<<<< HEAD
 * Base WordPress Filesystem class which Filesystem implementations extend.
=======
 * Base WordPress Filesystem class for which Filesystem implementations extend
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
 *
 * @since 2.5.0
 */
class WP_Filesystem_Base {
<<<<<<< HEAD

=======
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	/**
	 * Whether to display debug data for the connection.
	 *
	 * @since 2.5.0
	 * @var bool
	 */
	public $verbose = false;

	/**
	 * Cached list of local filepaths to mapped remote filepaths.
	 *
	 * @since 2.7.0
	 * @var array
	 */
	public $cache = array();

	/**
	 * The Access method of the current connection, Set automatically.
	 *
	 * @since 2.5.0
	 * @var string
	 */
	public $method = '';

	/**
	 * @var WP_Error
	 */
	public $errors = null;

	/**
	 */
	public $options = array();

	/**
<<<<<<< HEAD
	 * Returns the path on the remote filesystem of ABSPATH.
=======
	 * Return the path on the remote filesystem of ABSPATH.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.7.0
	 *
	 * @return string The location of the remote path.
	 */
	public function abspath() {
<<<<<<< HEAD
		$folder = $this->find_folder( ABSPATH );
		// Perhaps the FTP folder is rooted at the WordPress install, Check for wp-includes folder in root, Could have some false positives, but rare.
		if ( ! $folder && $this->is_dir( '/' . WPINC ) ) {
			$folder = '/';
		}
=======
		$folder = $this->find_folder(ABSPATH);
		// Perhaps the FTP folder is rooted at the WordPress install, Check for wp-includes folder in root, Could have some false positives, but rare.
		if ( ! $folder && $this->is_dir( '/' . WPINC ) )
			$folder = '/';
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return $folder;
	}

	/**
<<<<<<< HEAD
	 * Returns the path on the remote filesystem of WP_CONTENT_DIR.
=======
	 * Return the path on the remote filesystem of WP_CONTENT_DIR.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.7.0
	 *
	 * @return string The location of the remote path.
	 */
	public function wp_content_dir() {
<<<<<<< HEAD
		return $this->find_folder( WP_CONTENT_DIR );
	}

	/**
	 * Returns the path on the remote filesystem of WP_PLUGIN_DIR.
=======
		return $this->find_folder(WP_CONTENT_DIR);
	}

	/**
	 * Return the path on the remote filesystem of WP_PLUGIN_DIR.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.7.0
	 *
	 * @return string The location of the remote path.
	 */
	public function wp_plugins_dir() {
<<<<<<< HEAD
		return $this->find_folder( WP_PLUGIN_DIR );
	}

	/**
	 * Returns the path on the remote filesystem of the Themes Directory.
	 *
	 * @since 2.7.0
	 *
	 * @param string|false $theme Optional. The theme stylesheet or template for the directory.
	 *                            Default false.
=======
		return $this->find_folder(WP_PLUGIN_DIR);
	}

	/**
	 * Return the path on the remote filesystem of the Themes Directory.
	 *
	 * @since 2.7.0
	 *
	 * @param string $theme The Theme stylesheet or template for the directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 * @return string The location of the remote path.
	 */
	public function wp_themes_dir( $theme = false ) {
		$theme_root = get_theme_root( $theme );

		// Account for relative theme roots
<<<<<<< HEAD
		if ( '/themes' == $theme_root || ! is_dir( $theme_root ) ) {
			$theme_root = WP_CONTENT_DIR . $theme_root;
		}
=======
		if ( '/themes' == $theme_root || ! is_dir( $theme_root ) )
			$theme_root = WP_CONTENT_DIR . $theme_root;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		return $this->find_folder( $theme_root );
	}

	/**
<<<<<<< HEAD
	 * Returns the path on the remote filesystem of WP_LANG_DIR.
=======
	 * Return the path on the remote filesystem of WP_LANG_DIR.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 3.2.0
	 *
	 * @return string The location of the remote path.
	 */
	public function wp_lang_dir() {
<<<<<<< HEAD
		return $this->find_folder( WP_LANG_DIR );
	}

	/**
	 * Locates a folder on the remote filesystem.
=======
		return $this->find_folder(WP_LANG_DIR);
	}

	/**
	 * Locate a folder on the remote filesystem.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @deprecated 2.7.0 use WP_Filesystem::abspath() or WP_Filesystem::wp_*_dir() instead.
	 * @see WP_Filesystem::abspath()
	 * @see WP_Filesystem::wp_content_dir()
	 * @see WP_Filesystem::wp_plugins_dir()
	 * @see WP_Filesystem::wp_themes_dir()
	 * @see WP_Filesystem::wp_lang_dir()
	 *
	 * @param string $base The folder to start searching from.
	 * @param bool   $echo True to display debug information.
	 *                     Default false.
	 * @return string The location of the remote path.
	 */
	public function find_base_dir( $base = '.', $echo = false ) {
<<<<<<< HEAD
		_deprecated_function( __FUNCTION__, '2.7.0', 'WP_Filesystem::abspath() or WP_Filesystem::wp_*_dir()' );
=======
		_deprecated_function(__FUNCTION__, '2.7.0', 'WP_Filesystem::abspath() or WP_Filesystem::wp_*_dir()' );
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		$this->verbose = $echo;
		return $this->abspath();
	}

	/**
<<<<<<< HEAD
	 * Locates a folder on the remote filesystem.
=======
	 * Locate a folder on the remote filesystem.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @deprecated 2.7.0 use WP_Filesystem::abspath() or WP_Filesystem::wp_*_dir() methods instead.
	 * @see WP_Filesystem::abspath()
	 * @see WP_Filesystem::wp_content_dir()
	 * @see WP_Filesystem::wp_plugins_dir()
	 * @see WP_Filesystem::wp_themes_dir()
	 * @see WP_Filesystem::wp_lang_dir()
	 *
	 * @param string $base The folder to start searching from.
	 * @param bool   $echo True to display debug information.
	 * @return string The location of the remote path.
	 */
	public function get_base_dir( $base = '.', $echo = false ) {
<<<<<<< HEAD
		_deprecated_function( __FUNCTION__, '2.7.0', 'WP_Filesystem::abspath() or WP_Filesystem::wp_*_dir()' );
=======
		_deprecated_function(__FUNCTION__, '2.7.0', 'WP_Filesystem::abspath() or WP_Filesystem::wp_*_dir()' );
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		$this->verbose = $echo;
		return $this->abspath();
	}

	/**
<<<<<<< HEAD
	 * Locates a folder on the remote filesystem.
	 *
	 * Assumes that on Windows systems, Stripping off the Drive
	 * letter is OK Sanitizes \\ to / in Windows filepaths.
=======
	 * Locate a folder on the remote filesystem.
	 *
	 * Assumes that on Windows systems, Stripping off the Drive
	 * letter is OK Sanitizes \\ to / in windows filepaths.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.7.0
	 *
	 * @param string $folder the folder to locate.
	 * @return string|false The location of the remote path, false on failure.
	 */
	public function find_folder( $folder ) {
<<<<<<< HEAD
		if ( isset( $this->cache[ $folder ] ) ) {
			return $this->cache[ $folder ];
		}

		if ( stripos( $this->method, 'ftp' ) !== false ) {
			$constant_overrides = array(
				'FTP_BASE'        => ABSPATH,
				'FTP_CONTENT_DIR' => WP_CONTENT_DIR,
				'FTP_PLUGIN_DIR'  => WP_PLUGIN_DIR,
				'FTP_LANG_DIR'    => WP_LANG_DIR,
=======
		if ( isset( $this->cache[ $folder ] ) )
			return $this->cache[ $folder ];

		if ( stripos($this->method, 'ftp') !== false ) {
			$constant_overrides = array(
				'FTP_BASE' => ABSPATH,
				'FTP_CONTENT_DIR' => WP_CONTENT_DIR,
				'FTP_PLUGIN_DIR' => WP_PLUGIN_DIR,
				'FTP_LANG_DIR' => WP_LANG_DIR
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
			);

			// Direct matches ( folder = CONSTANT/ )
			foreach ( $constant_overrides as $constant => $dir ) {
<<<<<<< HEAD
				if ( ! defined( $constant ) ) {
					continue;
				}
				if ( $folder === $dir ) {
					return trailingslashit( constant( $constant ) );
				}
=======
				if ( ! defined( $constant ) )
					continue;
				if ( $folder === $dir )
					return trailingslashit( constant( $constant ) );
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
			}

			// Prefix Matches ( folder = CONSTANT/subdir )
			foreach ( $constant_overrides as $constant => $dir ) {
<<<<<<< HEAD
				if ( ! defined( $constant ) ) {
					continue;
				}
=======
				if ( ! defined( $constant ) )
					continue;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
				if ( 0 === stripos( $folder, $dir ) ) { // $folder starts with $dir
					$potential_folder = preg_replace( '#^' . preg_quote( $dir, '#' ) . '/#i', trailingslashit( constant( $constant ) ), $folder );
					$potential_folder = trailingslashit( $potential_folder );

					if ( $this->is_dir( $potential_folder ) ) {
						$this->cache[ $folder ] = $potential_folder;
						return $potential_folder;
					}
				}
			}
		} elseif ( 'direct' == $this->method ) {
<<<<<<< HEAD
			$folder = str_replace( '\\', '/', $folder ); // Windows path sanitisation
			return trailingslashit( $folder );
		}

		$folder = preg_replace( '|^([a-z]{1}):|i', '', $folder ); // Strip out windows drive letter if it's there.
		$folder = str_replace( '\\', '/', $folder ); // Windows path sanitisation

		if ( isset( $this->cache[ $folder ] ) ) {
			return $this->cache[ $folder ];
		}

		if ( $this->exists( $folder ) ) { // Folder exists at that absolute path.
			$folder                 = trailingslashit( $folder );
			$this->cache[ $folder ] = $folder;
			return $folder;
		}
		if ( $return = $this->search_for_folder( $folder ) ) {
			$this->cache[ $folder ] = $return;
		}
=======
			$folder = str_replace('\\', '/', $folder); // Windows path sanitisation
			return trailingslashit($folder);
		}

		$folder = preg_replace('|^([a-z]{1}):|i', '', $folder); // Strip out windows drive letter if it's there.
		$folder = str_replace('\\', '/', $folder); // Windows path sanitisation

		if ( isset($this->cache[ $folder ] ) )
			return $this->cache[ $folder ];

		if ( $this->exists($folder) ) { // Folder exists at that absolute path.
			$folder = trailingslashit($folder);
			$this->cache[ $folder ] = $folder;
			return $folder;
		}
		if ( $return = $this->search_for_folder($folder) )
			$this->cache[ $folder ] = $return;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return $return;
	}

	/**
<<<<<<< HEAD
	 * Locates a folder on the remote filesystem.
=======
	 * Locate a folder on the remote filesystem.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * Expects Windows sanitized path.
	 *
	 * @since 2.7.0
	 *
	 * @param string $folder The folder to locate.
	 * @param string $base   The folder to start searching from.
	 * @param bool   $loop   If the function has recursed, Internal use only.
	 * @return string|false The location of the remote path, false to cease looping.
	 */
	public function search_for_folder( $folder, $base = '.', $loop = false ) {
<<<<<<< HEAD
		if ( empty( $base ) || '.' == $base ) {
			$base = trailingslashit( $this->cwd() );
		}

		$folder = untrailingslashit( $folder );
=======
		if ( empty( $base ) || '.' == $base )
			$base = trailingslashit($this->cwd());

		$folder = untrailingslashit($folder);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		if ( $this->verbose ) {
			/* translators: 1: folder to locate, 2: folder to start searching from */
			printf( "\n" . __( 'Looking for %1$s in %2$s' ) . "<br/>\n", $folder, $base );
		}

<<<<<<< HEAD
		$folder_parts     = explode( '/', $folder );
		$folder_part_keys = array_keys( $folder_parts );
		$last_index       = array_pop( $folder_part_keys );
		$last_path        = $folder_parts[ $last_index ];
=======
		$folder_parts = explode('/', $folder);
		$folder_part_keys = array_keys( $folder_parts );
		$last_index = array_pop( $folder_part_keys );
		$last_path = $folder_parts[ $last_index ];
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		$files = $this->dirlist( $base );

		foreach ( $folder_parts as $index => $key ) {
<<<<<<< HEAD
			if ( $index == $last_index ) {
				continue; // We want this to be caught by the next code block.
			}
=======
			if ( $index == $last_index )
				continue; // We want this to be caught by the next code block.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

			/*
			 * Working from /home/ to /user/ to /wordpress/ see if that file exists within
			 * the current folder, If it's found, change into it and follow through looking
<<<<<<< HEAD
			 * for it. If it can't find WordPress down that route, it'll continue onto the next
			 * folder level, and see if that matches, and so on. If it reaches the end, and still
			 * can't find it, it'll return false for the entire function.
			 */
			if ( isset( $files[ $key ] ) ) {

				// Let's try that folder:
				$newdir = trailingslashit( path_join( $base, $key ) );
=======
			 * for it. If it cant find WordPress down that route, it'll continue onto the next
			 * folder level, and see if that matches, and so on. If it reaches the end, and still
			 * cant find it, it'll return false for the entire function.
			 */
			if ( isset($files[ $key ]) ){

				// Let's try that folder:
				$newdir = trailingslashit(path_join($base, $key));
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
				if ( $this->verbose ) {
					/* translators: %s: directory name */
					printf( "\n" . __( 'Changing to %s' ) . "<br/>\n", $newdir );
				}

				// Only search for the remaining path tokens in the directory, not the full path again.
				$newfolder = implode( '/', array_slice( $folder_parts, $index + 1 ) );
<<<<<<< HEAD
				if ( $ret = $this->search_for_folder( $newfolder, $newdir, $loop ) ) {
					return $ret;
				}
=======
				if ( $ret = $this->search_for_folder( $newfolder, $newdir, $loop) )
					return $ret;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
			}
		}

		// Only check this as a last resort, to prevent locating the incorrect install.
		// All above procedures will fail quickly if this is the right branch to take.
<<<<<<< HEAD
		if ( isset( $files[ $last_path ] ) ) {
			if ( $this->verbose ) {
				/* translators: %s: directory name */
				printf( "\n" . __( 'Found %s' ) . "<br/>\n", $base . $last_path );
			}
			return trailingslashit( $base . $last_path );
=======
		if (isset( $files[ $last_path ] ) ) {
			if ( $this->verbose ) {
				/* translators: %s: directory name */
				printf( "\n" . __( 'Found %s' ) . "<br/>\n",  $base . $last_path );
			}
			return trailingslashit($base . $last_path);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		}

		// Prevent this function from looping again.
		// No need to proceed if we've just searched in /
<<<<<<< HEAD
		if ( $loop || '/' == $base ) {
			return false;
		}
=======
		if ( $loop || '/' == $base )
			return false;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		// As an extra last resort, Change back to / if the folder wasn't found.
		// This comes into effect when the CWD is /home/user/ but WP is at /var/www/....
		return $this->search_for_folder( $folder, '/', true );

	}

	/**
<<<<<<< HEAD
	 * Returns the *nix-style file permissions for a file.
=======
	 * Return the *nix-style file permissions for a file.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * From the PHP documentation page for fileperms().
	 *
	 * @link https://secure.php.net/manual/en/function.fileperms.php
	 *
	 * @since 2.5.0
	 *
	 * @param string $file String filename.
	 * @return string The *nix-style representation of permissions.
	 */
<<<<<<< HEAD
	public function gethchmod( $file ) {
		$perms = intval( $this->getchmod( $file ), 8 );
		if ( ( $perms & 0xC000 ) == 0xC000 ) { // Socket
			$info = 's';
		} elseif ( ( $perms & 0xA000 ) == 0xA000 ) { // Symbolic Link
			$info = 'l';
		} elseif ( ( $perms & 0x8000 ) == 0x8000 ) { // Regular
			$info = '-';
		} elseif ( ( $perms & 0x6000 ) == 0x6000 ) { // Block special
			$info = 'b';
		} elseif ( ( $perms & 0x4000 ) == 0x4000 ) { // Directory
			$info = 'd';
		} elseif ( ( $perms & 0x2000 ) == 0x2000 ) { // Character special
			$info = 'c';
		} elseif ( ( $perms & 0x1000 ) == 0x1000 ) { // FIFO pipe
			$info = 'p';
		} else { // Unknown
			$info = 'u';
		}

		// Owner
		$info .= ( ( $perms & 0x0100 ) ? 'r' : '-' );
		$info .= ( ( $perms & 0x0080 ) ? 'w' : '-' );
		$info .= ( ( $perms & 0x0040 ) ?
					( ( $perms & 0x0800 ) ? 's' : 'x' ) :
					( ( $perms & 0x0800 ) ? 'S' : '-' ) );

		// Group
		$info .= ( ( $perms & 0x0020 ) ? 'r' : '-' );
		$info .= ( ( $perms & 0x0010 ) ? 'w' : '-' );
		$info .= ( ( $perms & 0x0008 ) ?
					( ( $perms & 0x0400 ) ? 's' : 'x' ) :
					( ( $perms & 0x0400 ) ? 'S' : '-' ) );

		// World
		$info .= ( ( $perms & 0x0004 ) ? 'r' : '-' );
		$info .= ( ( $perms & 0x0002 ) ? 'w' : '-' );
		$info .= ( ( $perms & 0x0001 ) ?
					( ( $perms & 0x0200 ) ? 't' : 'x' ) :
					( ( $perms & 0x0200 ) ? 'T' : '-' ) );
=======
	public function gethchmod( $file ){
		$perms = intval( $this->getchmod( $file ), 8 );
		if (($perms & 0xC000) == 0xC000) // Socket
			$info = 's';
		elseif (($perms & 0xA000) == 0xA000) // Symbolic Link
			$info = 'l';
		elseif (($perms & 0x8000) == 0x8000) // Regular
			$info = '-';
		elseif (($perms & 0x6000) == 0x6000) // Block special
			$info = 'b';
		elseif (($perms & 0x4000) == 0x4000) // Directory
			$info = 'd';
		elseif (($perms & 0x2000) == 0x2000) // Character special
			$info = 'c';
		elseif (($perms & 0x1000) == 0x1000) // FIFO pipe
			$info = 'p';
		else // Unknown
			$info = 'u';

		// Owner
		$info .= (($perms & 0x0100) ? 'r' : '-');
		$info .= (($perms & 0x0080) ? 'w' : '-');
		$info .= (($perms & 0x0040) ?
					(($perms & 0x0800) ? 's' : 'x' ) :
					(($perms & 0x0800) ? 'S' : '-'));

		// Group
		$info .= (($perms & 0x0020) ? 'r' : '-');
		$info .= (($perms & 0x0010) ? 'w' : '-');
		$info .= (($perms & 0x0008) ?
					(($perms & 0x0400) ? 's' : 'x' ) :
					(($perms & 0x0400) ? 'S' : '-'));

		// World
		$info .= (($perms & 0x0004) ? 'r' : '-');
		$info .= (($perms & 0x0002) ? 'w' : '-');
		$info .= (($perms & 0x0001) ?
					(($perms & 0x0200) ? 't' : 'x' ) :
					(($perms & 0x0200) ? 'T' : '-'));
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return $info;
	}

	/**
<<<<<<< HEAD
	 * Gets the permissions of the specified file or filepath in their octal format.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to the file.
	 * @return string Mode of the file (the last 3 digits).
=======
	 * Gets the permissions of the specified file or filepath in their octal format
	 *
	 * @since 2.5.0
	 * @param string $file
	 * @return string the last 3 characters of the octal number
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function getchmod( $file ) {
		return '777';
	}

	/**
<<<<<<< HEAD
	 * Converts *nix-style file permissions to a octal number.
	 *
	 * Converts '-rw-r--r--' to 0644
	 * From "info at rvgate dot nl"'s comment on the PHP documentation for chmod()
	 *
=======
	 * Convert *nix-style file permissions to a octal number.
	 *
	 * Converts '-rw-r--r--' to 0644
	 * From "info at rvgate dot nl"'s comment on the PHP documentation for chmod()
 	 *
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 * @link https://secure.php.net/manual/en/function.chmod.php#49614
	 *
	 * @since 2.5.0
	 *
	 * @param string $mode string The *nix-style file permission.
	 * @return int octal representation
	 */
	public function getnumchmodfromh( $mode ) {
		$realmode = '';
<<<<<<< HEAD
		$legal    = array( '', 'w', 'r', 'x', '-' );
		$attarray = preg_split( '//', $mode );

		for ( $i = 0, $c = count( $attarray ); $i < $c; $i++ ) {
			if ( $key = array_search( $attarray[ $i ], $legal ) ) {
				$realmode .= $legal[ $key ];
			}
		}

		$mode  = str_pad( $realmode, 10, '-', STR_PAD_LEFT );
		$trans = array(
			'-' => '0',
			'r' => '4',
			'w' => '2',
			'x' => '1',
		);
		$mode  = strtr( $mode, $trans );

		$newmode  = $mode[0];
=======
		$legal =  array('', 'w', 'r', 'x', '-');
		$attarray = preg_split('//', $mode);

		for ( $i = 0, $c = count( $attarray ); $i < $c; $i++ ) {
		   if ($key = array_search($attarray[$i], $legal)) {
			   $realmode .= $legal[$key];
		   }
		}

		$mode = str_pad($realmode, 10, '-', STR_PAD_LEFT);
		$trans = array('-'=>'0', 'r'=>'4', 'w'=>'2', 'x'=>'1');
		$mode = strtr($mode,$trans);

		$newmode = $mode[0];
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		$newmode .= $mode[1] + $mode[2] + $mode[3];
		$newmode .= $mode[4] + $mode[5] + $mode[6];
		$newmode .= $mode[7] + $mode[8] + $mode[9];
		return $newmode;
	}

	/**
<<<<<<< HEAD
	 * Determines if the string provided contains binary characters.
=======
	 * Determine if the string provided contains binary characters.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.7.0
	 *
	 * @param string $text String to test against.
<<<<<<< HEAD
	 * @return bool True if string is binary, false otherwise.
=======
	 * @return bool true if string is binary, false otherwise.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function is_binary( $text ) {
		return (bool) preg_match( '|[^\x20-\x7E]|', $text ); // chr(32)..chr(127)
	}

	/**
<<<<<<< HEAD
	 * Changes the owner of a file or directory.
=======
	 * Change the ownership of a file / folder.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * Default behavior is to do nothing, override this in your subclass, if desired.
	 *
	 * @since 2.5.0
	 *
<<<<<<< HEAD
	 * @param string     $file      Path to the file or directory.
	 * @param string|int $owner     A user name or number.
	 * @param bool       $recursive Optional. If set to true, changes file owner recursively.
	 *                              Default false.
	 * @return bool True on success, false on failure.
=======
	 * @param string $file      Path to the file.
	 * @param mixed  $owner     A user name or number.
	 * @param bool   $recursive Optional. If set True changes file owner recursivly. Defaults to False.
	 * @return bool Returns true on success or false on failure.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function chown( $file, $owner, $recursive = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Connects filesystem.
=======
	 * Connect filesystem.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @return bool True on success, false on failure (always true for WP_Filesystem_Direct).
=======
	 * @return bool True on success or false on failure (always true for WP_Filesystem_Direct).
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function connect() {
		return true;
	}

	/**
<<<<<<< HEAD
	 * Reads entire file into a string.
=======
	 * Read entire file into a string.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file Name of the file to read.
<<<<<<< HEAD
	 * @return string|false Read data on success, false on failure.
=======
	 * @return mixed|bool Returns the read data or false on failure.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function get_contents( $file ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Reads entire file into an array.
=======
	 * Read entire file into an array.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file Path to the file.
<<<<<<< HEAD
	 * @return array|false File contents in an array on success, false on failure.
=======
	 * @return array|bool the file contents in an array or false on failure.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function get_contents_array( $file ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Writes a string to a file.
=======
	 * Write a string to a file.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @param string    $file     Remote path to the file where to write the data.
	 * @param string    $contents The data to write.
	 * @param int|false $mode     Optional. The file permissions as octal number, usually 0644.
	 *                            Default false.
	 * @return bool True on success, false on failure.
=======
	 * @param string $file     Remote path to the file where to write the data.
	 * @param string $contents The data to write.
	 * @param int    $mode     Optional. The file permissions as octal number, usually 0644.
	 * @return bool False on failure.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function put_contents( $file, $contents, $mode = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Gets the current working directory.
=======
	 * Get the current working directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @return string|false The current working directory on success, false on failure.
=======
	 * @return string|bool The current working directory on success, or false on failure.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function cwd() {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Changes current directory.
=======
	 * Change current directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $dir The new current directory.
<<<<<<< HEAD
	 * @return bool True on success, false on failure.
=======
	 * @return bool|string
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function chdir( $dir ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Changes the file group.
=======
	 * Change the file group.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @param string     $file      Path to the file.
	 * @param string|int $group     A group name or number.
	 * @param bool       $recursive Optional. If set to true, changes file group recursively.
	 *                              Default false.
	 * @return bool True on success, false on failure.
=======
	 * @param string $file      Path to the file.
	 * @param mixed  $group     A group name or number.
	 * @param bool   $recursive Optional. If set True changes file group recursively. Defaults to False.
	 * @return bool|string
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function chgrp( $file, $group, $recursive = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Changes filesystem permissions.
=======
	 * Change filesystem permissions.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @param string    $file      Path to the file.
	 * @param int|false $mode      Optional. The permissions as octal number, usually 0644 for files,
	 *                             0755 for directories. Default false.
	 * @param bool      $recursive Optional. If set to true, changes file group recursively.
	 *                             Default false.
	 * @return bool True on success, false on failure.
=======
	 * @param string $file      Path to the file.
	 * @param int    $mode      Optional. The permissions as octal number, usually 0644 for files, 0755 for dirs.
	 * @param bool   $recursive Optional. If set True changes file group recursively. Defaults to False.
	 * @return bool|string
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function chmod( $file, $mode = false, $recursive = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Gets the file owner.
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file Path to the file.
	 * @return string|false Username of the owner on success, false on failure.
=======
	 * Get the file owner.
	 *
	 * @since 2.5.0
	 * @abstract
	 * 
	 * @param string $file Path to the file.
	 * @return string|bool Username of the user or false on error.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function owner( $file ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Gets the file's group.
=======
	 * Get the file's group.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file Path to the file.
<<<<<<< HEAD
	 * @return string|false The group on success, false on failure.
=======
	 * @return string|bool The group or false on error.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function group( $file ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Copies a file.
=======
	 * Copy a file.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @param string    $source      Path to the source file.
	 * @param string    $destination Path to the destination file.
	 * @param bool      $overwrite   Optional. Whether to overwrite the destination file if it exists.
	 *                               Default false.
	 * @param int|false $mode        Optional. The permissions as octal number, usually 0644 for files,
	 *                               0755 for dirs. Default false.
	 * @return bool True on success, false on failure.
=======
	 * @param string $source      Path to the source file.
	 * @param string $destination Path to the destination file.
	 * @param bool   $overwrite   Optional. Whether to overwrite the destination file if it exists.
	 *                            Default false.
	 * @param int    $mode        Optional. The permissions as octal number, usually 0644 for files, 0755 for dirs.
	 *                            Default false.
	 * @return bool True if file copied successfully, False otherwise.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function copy( $source, $destination, $overwrite = false, $mode = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Moves a file.
=======
	 * Move a file.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $source      Path to the source file.
	 * @param string $destination Path to the destination file.
	 * @param bool   $overwrite   Optional. Whether to overwrite the destination file if it exists.
	 *                            Default false.
<<<<<<< HEAD
	 * @return bool True on success, false on failure.
=======
	 * @return bool True if file copied successfully, False otherwise.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function move( $source, $destination, $overwrite = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Deletes a file or directory.
=======
	 * Delete a file or directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @param string       $file      Path to the file or directory.
	 * @param bool         $recursive Optional. If set to true, changes file group recursively.
	 *                                Default false.
	 * @param string|false $type      Type of resource. 'f' for file, 'd' for directory.
	 *                                Default false.
	 * @return bool True on success, false on failure.
=======
	 * @param string $file      Path to the file.
	 * @param bool   $recursive Optional. If set True changes file group recursively. Defaults to False.
	 *                          Default false.
	 * @param bool   $type      Type of resource. 'f' for file, 'd' for directory.
	 *                          Default false.
	 * @return bool True if the file or directory was deleted, false on failure.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function delete( $file, $recursive = false, $type = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Checks if a file or directory exists.
=======
	 * Check if a file or directory exists.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @param string $file Path to file or directory.
=======
	 * @param string $file Path to file/directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 * @return bool Whether $file exists or not.
	 */
	public function exists( $file ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Checks if resource is a file.
=======
	 * Check if resource is a file.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file File path.
	 * @return bool Whether $file is a file.
	 */
	public function is_file( $file ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Checks if resource is a directory.
=======
	 * Check if resource is a directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $path Directory path.
	 * @return bool Whether $path is a directory.
	 */
	public function is_dir( $path ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Checks if a file is readable.
=======
	 * Check if a file is readable.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file Path to file.
	 * @return bool Whether $file is readable.
	 */
	public function is_readable( $file ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Checks if a file or directory is writable.
=======
	 * Check if a file or directory is writable.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @param string $file Path to file or directory.
=======
	 * @param string $file Path to file.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 * @return bool Whether $file is writable.
	 */
	public function is_writable( $file ) {
		return false;
	}

	/**
	 * Gets the file's last access time.
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file Path to file.
<<<<<<< HEAD
	 * @return int|false Unix timestamp representing last access time, false on failure.
=======
	 * @return int|bool Unix timestamp representing last access time.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function atime( $file ) {
		return false;
	}

	/**
	 * Gets the file modification time.
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file Path to file.
<<<<<<< HEAD
	 * @return int|false Unix timestamp representing modification time, false on failure.
=======
	 * @return int|bool Unix timestamp representing modification time.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function mtime( $file ) {
		return false;
	}

	/**
	 * Gets the file size (in bytes).
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file Path to file.
<<<<<<< HEAD
	 * @return int|false Size of the file in bytes on success, false on failure.
=======
	 * @return int|bool Size of the file in bytes.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function size( $file ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Sets the access and modification times of a file.
=======
	 * Set the access and modification times of a file.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * Note: If $file doesn't exist, it will be created.
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $file  Path to file.
	 * @param int    $time  Optional. Modified time to set for file.
	 *                      Default 0.
	 * @param int    $atime Optional. Access time to set for file.
	 *                      Default 0.
<<<<<<< HEAD
	 * @return bool True on success, false on failure.
=======
	 * @return bool Whether operation was successful or not.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function touch( $file, $time = 0, $atime = 0 ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Creates a directory.
=======
	 * Create a directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
<<<<<<< HEAD
	 * @param string     $path  Path for new directory.
	 * @param int|false  $chmod Optional. The permissions as octal number (or false to skip chmod).
	 *                          Default false.
	 * @param string|int $chown Optional. A user name or number (or false to skip chown).
	 *                          Default false.
	 * @param string|int $chgrp Optional. A group name or number (or false to skip chgrp).
	 *                          Default false.
	 * @return bool True on success, false on failure.
=======
	 * @param string $path  Path for new directory.
	 * @param mixed  $chmod Optional. The permissions as octal number, (or False to skip chmod)
	 *                      Default false.
	 * @param mixed  $chown Optional. A user name or number (or False to skip chown)
	 *                      Default false.
	 * @param mixed  $chgrp Optional. A group name or number (or False to skip chgrp).
	 *                      Default false.
	 * @return bool False if directory cannot be created, true otherwise.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function mkdir( $path, $chmod = false, $chown = false, $chgrp = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Deletes a directory.
=======
	 * Delete a directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $path      Path to directory.
	 * @param bool   $recursive Optional. Whether to recursively remove files/directories.
	 *                          Default false.
<<<<<<< HEAD
	 * @return bool True on success, false on failure.
=======
	 * @return bool Whether directory is deleted successfully or not.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function rmdir( $path, $recursive = false ) {
		return false;
	}

	/**
<<<<<<< HEAD
	 * Gets details for files in a directory or a specific file.
=======
	 * Get details for files in a directory or a specific file.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *
	 * @since 2.5.0
	 * @abstract
	 *
	 * @param string $path           Path to directory or file.
	 * @param bool   $include_hidden Optional. Whether to include details of hidden ("." prefixed) files.
	 *                               Default true.
	 * @param bool   $recursive      Optional. Whether to recursively include file details in nested directories.
	 *                               Default false.
<<<<<<< HEAD
	 * @return array|false {
	 *     Array of files. False if unable to list directory contents.
	 *
	 *     @type string $name        Name of the file or directory.
=======
	 * @return array|bool {
	 *     Array of files. False if unable to list directory contents.
	 *
	 *     @type string $name        Name of the file/directory.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 *     @type string $perms       *nix representation of permissions.
	 *     @type int    $permsn      Octal representation of permissions.
	 *     @type string $owner       Owner name or ID.
	 *     @type int    $size        Size of file in bytes.
	 *     @type int    $lastmodunix Last modified unix timestamp.
	 *     @type mixed  $lastmod     Last modified month (3 letter) and day (without leading 0).
	 *     @type int    $time        Last modified time.
	 *     @type string $type        Type of resource. 'f' for file, 'd' for directory.
	 *     @type mixed  $files       If a directory and $recursive is true, contains another array of files.
	 * }
	 */
	public function dirlist( $path, $include_hidden = true, $recursive = false ) {
		return false;
	}

} // WP_Filesystem_Base
