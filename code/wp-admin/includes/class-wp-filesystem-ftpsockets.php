<?php
/**
 * WordPress FTP Sockets Filesystem.
 *
 * @package WordPress
 * @subpackage Filesystem
 */

/**
 * WordPress Filesystem Class for implementing FTP Sockets.
 *
 * @since 2.5.0
 *
 * @see WP_Filesystem_Base
 */
class WP_Filesystem_ftpsockets extends WP_Filesystem_Base {
<<<<<<< HEAD

	/**
	 * @since 2.5.0
=======
	/**
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 * @var ftp
	 */
	public $ftp;

	/**
<<<<<<< HEAD
	 * Constructor.
	 *
	 * @since 2.5.0
	 *
	 * @param array $opt
	 */
	public function __construct( $opt = '' ) {
=======
	 *
	 * @param array $opt
	 */
	public function __construct( $opt  = '' ) {
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		$this->method = 'ftpsockets';
		$this->errors = new WP_Error();

		// Check if possible to use ftp functions.
		if ( ! @include_once( ABSPATH . 'wp-admin/includes/class-ftp.php' ) ) {
			return;
		}
		$this->ftp = new ftp();

<<<<<<< HEAD
		if ( empty( $opt['port'] ) ) {
			$this->options['port'] = 21;
		} else {
			$this->options['port'] = (int) $opt['port'];
		}

		if ( empty( $opt['hostname'] ) ) {
			$this->errors->add( 'empty_hostname', __( 'FTP hostname is required' ) );
		} else {
			$this->options['hostname'] = $opt['hostname'];
		}

		// Check if the options provided are OK.
		if ( empty( $opt['username'] ) ) {
			$this->errors->add( 'empty_username', __( 'FTP username is required' ) );
		} else {
			$this->options['username'] = $opt['username'];
		}

		if ( empty( $opt['password'] ) ) {
			$this->errors->add( 'empty_password', __( 'FTP password is required' ) );
		} else {
			$this->options['password'] = $opt['password'];
		}
	}

	/**
	 * Connects filesystem.
	 *
	 * @since 2.5.0
	 *
	 * @return bool True on success, false on failure.
	 */
	public function connect() {
		if ( ! $this->ftp ) {
			return false;
		}

		$this->ftp->setTimeout( FS_CONNECT_TIMEOUT );

		if ( ! $this->ftp->SetServer( $this->options['hostname'], $this->options['port'] ) ) {
			$this->errors->add(
				'connect',
				/* translators: %s: hostname:port */
				sprintf(
					__( 'Failed to connect to FTP Server %s' ),
=======
		if ( empty($opt['port']) )
			$this->options['port'] = 21;
		else
			$this->options['port'] = (int) $opt['port'];

		if ( empty($opt['hostname']) )
			$this->errors->add('empty_hostname', __('FTP hostname is required'));
		else
			$this->options['hostname'] = $opt['hostname'];

		// Check if the options provided are OK.
		if ( empty ($opt['username']) )
			$this->errors->add('empty_username', __('FTP username is required'));
		else
			$this->options['username'] = $opt['username'];

		if ( empty ($opt['password']) )
			$this->errors->add('empty_password', __('FTP password is required'));
		else
			$this->options['password'] = $opt['password'];
	}

	/**
	 *
	 * @return bool
	 */
	public function connect() {
		if ( ! $this->ftp )
			return false;

		$this->ftp->setTimeout(FS_CONNECT_TIMEOUT);

		if ( ! $this->ftp->SetServer( $this->options['hostname'], $this->options['port'] ) ) {
			$this->errors->add( 'connect',
				/* translators: %s: hostname:port */
				sprintf( __( 'Failed to connect to FTP Server %s' ),
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
					$this->options['hostname'] . ':' . $this->options['port']
				)
			);
			return false;
		}

		if ( ! $this->ftp->connect() ) {
<<<<<<< HEAD
			$this->errors->add(
				'connect',
				/* translators: %s: hostname:port */
				sprintf(
					__( 'Failed to connect to FTP Server %s' ),
=======
			$this->errors->add( 'connect',
				/* translators: %s: hostname:port */
				sprintf( __( 'Failed to connect to FTP Server %s' ),
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
					$this->options['hostname'] . ':' . $this->options['port']
				)
			);
			return false;
		}

		if ( ! $this->ftp->login( $this->options['username'], $this->options['password'] ) ) {
<<<<<<< HEAD
			$this->errors->add(
				'auth',
				/* translators: %s: username */
				sprintf(
					__( 'Username/Password incorrect for %s' ),
=======
			$this->errors->add( 'auth',
				/* translators: %s: username */
				sprintf( __( 'Username/Password incorrect for %s' ),
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
					$this->options['username']
				)
			);
			return false;
		}

		$this->ftp->SetType( FTP_BINARY );
		$this->ftp->Passive( true );
		$this->ftp->setTimeout( FS_TIMEOUT );
		return true;
	}

	/**
<<<<<<< HEAD
	 * Reads entire file into a string.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Name of the file to read.
	 * @return string|false Read data on success, false if no temporary file could be opened,
	 *                      or if the file couldn't be retrieved.
	 */
	public function get_contents( $file ) {
		if ( ! $this->exists( $file ) ) {
			return false;
		}
=======
	 * Retrieves the file contents.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Filename.
	 * @return string|false File contents on success, false if no temp file could be opened,
	 *                      or if the file doesn't exist.
	 */
	public function get_contents( $file ) {
		if ( ! $this->exists($file) )
			return false;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		$temp = wp_tempnam( $file );

		if ( ! $temphandle = fopen( $temp, 'w+' ) ) {
			unlink( $temp );
			return false;
		}

		mbstring_binary_safe_encoding();

<<<<<<< HEAD
		if ( ! $this->ftp->fget( $temphandle, $file ) ) {
			fclose( $temphandle );
			unlink( $temp );
=======
		if ( ! $this->ftp->fget($temphandle, $file) ) {
			fclose($temphandle);
			unlink($temp);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

			reset_mbstring_encoding();

			return ''; // Blank document, File does exist, It's just blank.
		}

		reset_mbstring_encoding();

		fseek( $temphandle, 0 ); // Skip back to the start of the file being written to
		$contents = '';

<<<<<<< HEAD
		while ( ! feof( $temphandle ) ) {
			$contents .= fread( $temphandle, 8 * KB_IN_BYTES );
		}

		fclose( $temphandle );
		unlink( $temp );
=======
		while ( ! feof($temphandle) )
			$contents .= fread($temphandle, 8192);

		fclose($temphandle);
		unlink($temp);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return $contents;
	}

	/**
<<<<<<< HEAD
	 * Reads entire file into an array.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to the file.
	 * @return array|false File contents in an array on success, false on failure.
	 */
	public function get_contents_array( $file ) {
		return explode( "\n", $this->get_contents( $file ) );
	}

	/**
	 * Writes a string to a file.
	 *
	 * @since 2.5.0
	 *
	 * @param string    $file     Remote path to the file where to write the data.
	 * @param string    $contents The data to write.
	 * @param int|false $mode     Optional. The file permissions as octal number, usually 0644.
	 *                            Default false.
	 * @return bool True on success, false on failure.
	 */
	public function put_contents( $file, $contents, $mode = false ) {
		$temp = wp_tempnam( $file );
		if ( ! $temphandle = @fopen( $temp, 'w+' ) ) {
			unlink( $temp );
=======
	 *
	 * @param string $file
	 * @return array
	 */
	public function get_contents_array($file) {
		return explode("\n", $this->get_contents($file) );
	}

	/**
	 *
	 * @param string $file
	 * @param string $contents
	 * @param int|bool $mode
	 * @return bool
	 */
	public function put_contents($file, $contents, $mode = false ) {
		$temp = wp_tempnam( $file );
		if ( ! $temphandle = @fopen($temp, 'w+') ) {
			unlink($temp);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
			return false;
		}

		// The FTP class uses string functions internally during file download/upload
		mbstring_binary_safe_encoding();

		$bytes_written = fwrite( $temphandle, $contents );
		if ( false === $bytes_written || $bytes_written != strlen( $contents ) ) {
			fclose( $temphandle );
			unlink( $temp );

			reset_mbstring_encoding();

			return false;
		}

		fseek( $temphandle, 0 ); // Skip back to the start of the file being written to

<<<<<<< HEAD
		$ret = $this->ftp->fput( $file, $temphandle );

		reset_mbstring_encoding();

		fclose( $temphandle );
		unlink( $temp );

		$this->chmod( $file, $mode );
=======
		$ret = $this->ftp->fput($file, $temphandle);

		reset_mbstring_encoding();

		fclose($temphandle);
		unlink($temp);

		$this->chmod($file, $mode);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

		return $ret;
	}

	/**
<<<<<<< HEAD
	 * Gets the current working directory.
	 *
	 * @since 2.5.0
	 *
	 * @return string|false The current working directory on success, false on failure.
	 */
	public function cwd() {
		$cwd = $this->ftp->pwd();
		if ( $cwd ) {
			$cwd = trailingslashit( $cwd );
		}
=======
	 *
	 * @return string
	 */
	public function cwd() {
		$cwd = $this->ftp->pwd();
		if ( $cwd )
			$cwd = trailingslashit($cwd);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return $cwd;
	}

	/**
<<<<<<< HEAD
	 * Changes current directory.
	 *
	 * @since 2.5.0
	 *
	 * @param string $dir The new current directory.
	 * @return bool True on success, false on failure.
	 */
	public function chdir( $dir ) {
		return $this->ftp->chdir( $dir );
	}

	/**
	 * Changes filesystem permissions.
	 *
	 * @since 2.5.0
	 *
	 * @param string    $file      Path to the file.
	 * @param int|false $mode      Optional. The permissions as octal number, usually 0644 for files,
	 *                             0755 for directories. Default false.
	 * @param bool      $recursive Optional. If set to true, changes file group recursively.
	 *                             Default false.
	 * @return bool True on success, false on failure.
	 */
	public function chmod( $file, $mode = false, $recursive = false ) {
		if ( ! $mode ) {
			if ( $this->is_file( $file ) ) {
				$mode = FS_CHMOD_FILE;
			} elseif ( $this->is_dir( $file ) ) {
				$mode = FS_CHMOD_DIR;
			} else {
				return false;
			}
		}

		// chmod any sub-objects if recursive.
		if ( $recursive && $this->is_dir( $file ) ) {
			$filelist = $this->dirlist( $file );
			foreach ( (array) $filelist as $filename => $filemeta ) {
				$this->chmod( $file . '/' . $filename, $mode, $recursive );
			}
		}

		// chmod the file or directory
		return $this->ftp->chmod( $file, $mode );
	}

	/**
	 * Gets the file owner.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to the file.
	 * @return string|false Username of the owner on success, false on failure.
	 */
	public function owner( $file ) {
		$dir = $this->dirlist( $file );
		return $dir[ $file ]['owner'];
	}

	/**
	 * Gets the permissions of the specified file or filepath in their octal format.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to the file.
	 * @return string Mode of the file (the last 3 digits).
	 */
	public function getchmod( $file ) {
		$dir = $this->dirlist( $file );
		return $dir[ $file ]['permsn'];
	}

	/**
	 * Gets the file's group.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to the file.
	 * @return string|false The group on success, false on failure.
	 */
	public function group( $file ) {
		$dir = $this->dirlist( $file );
		return $dir[ $file ]['group'];
	}

	/**
	 * Copies a file.
	 *
	 * @since 2.5.0
	 *
	 * @param string    $source      Path to the source file.
	 * @param string    $destination Path to the destination file.
	 * @param bool      $overwrite   Optional. Whether to overwrite the destination file if it exists.
	 *                               Default false.
	 * @param int|false $mode        Optional. The permissions as octal number, usually 0644 for files,
	 *                               0755 for dirs. Default false.
	 * @return bool True on success, false on failure.
	 */
	public function copy( $source, $destination, $overwrite = false, $mode = false ) {
		if ( ! $overwrite && $this->exists( $destination ) ) {
			return false;
		}

		$content = $this->get_contents( $source );
		if ( false === $content ) {
			return false;
		}

		return $this->put_contents( $destination, $content, $mode );
	}

	/**
	 * Moves a file.
	 *
	 * @since 2.5.0
	 *
	 * @param string $source      Path to the source file.
	 * @param string $destination Path to the destination file.
	 * @param bool   $overwrite   Optional. Whether to overwrite the destination file if it exists.
	 *                            Default false.
	 * @return bool True on success, false on failure.
	 */
	public function move( $source, $destination, $overwrite = false ) {
		return $this->ftp->rename( $source, $destination );
	}

	/**
	 * Deletes a file or directory.
	 *
	 * @since 2.5.0
	 *
	 * @param string       $file      Path to the file or directory.
	 * @param bool         $recursive Optional. If set to true, changes file group recursively.
	 *                                Default false.
	 * @param string|false $type      Type of resource. 'f' for file, 'd' for directory.
	 *                                Default false.
	 * @return bool True on success, false on failure.
	 */
	public function delete( $file, $recursive = false, $type = false ) {
		if ( empty( $file ) ) {
			return false;
		}
		if ( 'f' == $type || $this->is_file( $file ) ) {
			return $this->ftp->delete( $file );
		}
		if ( ! $recursive ) {
			return $this->ftp->rmdir( $file );
		}

		return $this->ftp->mdel( $file );
	}

	/**
	 * Checks if a file or directory exists.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to file or directory.
	 * @return bool Whether $file exists or not.
=======
	 *
	 * @param string $file
	 * @return bool
	 */
	public function chdir($file) {
		return $this->ftp->chdir($file);
	}

	/**
	 *
	 * @param string $file
	 * @param int|bool $mode
	 * @param bool $recursive
	 * @return bool
	 */
	public function chmod($file, $mode = false, $recursive = false ) {
		if ( ! $mode ) {
			if ( $this->is_file($file) )
				$mode = FS_CHMOD_FILE;
			elseif ( $this->is_dir($file) )
				$mode = FS_CHMOD_DIR;
			else
				return false;
		}

		// chmod any sub-objects if recursive.
		if ( $recursive && $this->is_dir($file) ) {
			$filelist = $this->dirlist($file);
			foreach ( (array)$filelist as $filename => $filemeta )
				$this->chmod($file . '/' . $filename, $mode, $recursive);
		}

		// chmod the file or directory
		return $this->ftp->chmod($file, $mode);
	}

	/**
	 *
	 * @param string $file
	 * @return string
	 */
	public function owner($file) {
		$dir = $this->dirlist($file);
		return $dir[$file]['owner'];
	}

	/**
	 *
	 * @param string $file
	 * @return string
	 */
	public function getchmod($file) {
		$dir = $this->dirlist($file);
		return $dir[$file]['permsn'];
	}

	/**
	 *
	 * @param string $file
	 * @return string
	 */
	public function group($file) {
		$dir = $this->dirlist($file);
		return $dir[$file]['group'];
	}

	/**
	 *
	 * @param string   $source
	 * @param string   $destination
	 * @param bool     $overwrite
	 * @param int|bool $mode
	 * @return bool
	 */
	public function copy($source, $destination, $overwrite = false, $mode = false) {
		if ( ! $overwrite && $this->exists($destination) )
			return false;

		$content = $this->get_contents($source);
		if ( false === $content )
			return false;

		return $this->put_contents($destination, $content, $mode);
	}

	/**
	 *
	 * @param string $source
	 * @param string $destination
	 * @param bool   $overwrite
	 * @return bool
	 */
	public function move($source, $destination, $overwrite = false ) {
		return $this->ftp->rename($source, $destination);
	}

	/**
	 *
	 * @param string $file
	 * @param bool   $recursive
	 * @param string $type
	 * @return bool
	 */
	public function delete($file, $recursive = false, $type = false) {
		if ( empty($file) )
			return false;
		if ( 'f' == $type || $this->is_file($file) )
			return $this->ftp->delete($file);
		if ( !$recursive )
			return $this->ftp->rmdir($file);

		return $this->ftp->mdel($file);
	}

	/**
	 *
	 * @param string $file
	 * @return bool
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function exists( $file ) {
		$list = $this->ftp->nlist( $file );

		if ( empty( $list ) && $this->is_dir( $file ) ) {
			return true; // File is an empty directory.
		}

<<<<<<< HEAD
		return ! empty( $list ); //empty list = no file, so invert.
=======
		return !empty( $list ); //empty list = no file, so invert.
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		// Return $this->ftp->is_exists($file); has issues with ABOR+426 responses on the ncFTPd server.
	}

	/**
<<<<<<< HEAD
	 * Checks if resource is a file.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file File path.
	 * @return bool Whether $file is a file.
	 */
	public function is_file( $file ) {
		if ( $this->is_dir( $file ) ) {
			return false;
		}
		if ( $this->exists( $file ) ) {
			return true;
		}
=======
	 *
	 * @param string $file
	 * @return bool
	 */
	public function is_file($file) {
		if ( $this->is_dir($file) )
			return false;
		if ( $this->exists($file) )
			return true;
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return false;
	}

	/**
<<<<<<< HEAD
	 * Checks if resource is a directory.
	 *
	 * @since 2.5.0
	 *
	 * @param string $path Directory path.
	 * @return bool Whether $path is a directory.
	 */
	public function is_dir( $path ) {
		$cwd = $this->cwd();
		if ( $this->chdir( $path ) ) {
			$this->chdir( $cwd );
=======
	 *
	 * @param string $path
	 * @return bool
	 */
	public function is_dir($path) {
		$cwd = $this->cwd();
		if ( $this->chdir($path) ) {
			$this->chdir($cwd);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
			return true;
		}
		return false;
	}

	/**
<<<<<<< HEAD
	 * Checks if a file is readable.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to file.
	 * @return bool Whether $file is readable.
	 */
	public function is_readable( $file ) {
=======
	 *
	 * @param string $file
	 * @return bool
	 */
	public function is_readable($file) {
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return true;
	}

	/**
<<<<<<< HEAD
	 * Checks if a file or directory is writable.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to file or directory.
	 * @return bool Whether $file is writable.
	 */
	public function is_writable( $file ) {
=======
	 *
	 * @param string $file
	 * @return bool
	 */
	public function is_writable($file) {
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return true;
	}

	/**
<<<<<<< HEAD
	 * Gets the file's last access time.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to file.
	 * @return int|false Unix timestamp representing last access time, false on failure.
	 */
	public function atime( $file ) {
=======
	 *
	 * @param string $file
	 * @return bool
	 */
	public function atime($file) {
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return false;
	}

	/**
<<<<<<< HEAD
	 * Gets the file modification time.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to file.
	 * @return int|false Unix timestamp representing modification time, false on failure.
	 */
	public function mtime( $file ) {
		return $this->ftp->mdtm( $file );
	}

	/**
	 * Gets the file size (in bytes).
	 *
	 * @since 2.5.0
	 *
	 * @param string $file Path to file.
	 * @return int|false Size of the file in bytes on success, false on failure.
	 */
	public function size( $file ) {
		return $this->ftp->filesize( $file );
	}

	/**
	 * Sets the access and modification times of a file.
	 *
	 * Note: If $file doesn't exist, it will be created.
	 *
	 * @since 2.5.0
	 *
	 * @param string $file  Path to file.
	 * @param int    $time  Optional. Modified time to set for file.
	 *                      Default 0.
	 * @param int    $atime Optional. Access time to set for file.
	 *                      Default 0.
	 * @return bool True on success, false on failure.
	 */
	public function touch( $file, $time = 0, $atime = 0 ) {
=======
	 *
	 * @param string $file
	 * @return int
	 */
	public function mtime($file) {
		return $this->ftp->mdtm($file);
	}

	/**
	 * @param string $file
	 * @return int
	 */
	public function size($file) {
		return $this->ftp->filesize($file);
	}

	/**
	 *
	 * @param string $file
	 * @param int $time
	 * @param int $atime
	 * @return bool
	 */
	public function touch($file, $time = 0, $atime = 0 ) {
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return false;
	}

	/**
<<<<<<< HEAD
	 * Creates a directory.
	 *
	 * @since 2.5.0
	 *
	 * @param string     $path  Path for new directory.
	 * @param int|false  $chmod Optional. The permissions as octal number (or false to skip chmod).
	 *                          Default false.
	 * @param string|int $chown Optional. A user name or number (or false to skip chown).
	 *                          Default false.
	 * @param string|int $chgrp Optional. A group name or number (or false to skip chgrp).
	 *                          Default false.
	 * @return bool True on success, false on failure.
	 */
	public function mkdir( $path, $chmod = false, $chown = false, $chgrp = false ) {
		$path = untrailingslashit( $path );
		if ( empty( $path ) ) {
			return false;
		}

		if ( ! $this->ftp->mkdir( $path ) ) {
			return false;
		}
		if ( ! $chmod ) {
			$chmod = FS_CHMOD_DIR;
		}
		$this->chmod( $path, $chmod );
=======
	 *
	 * @param string $path
	 * @param mixed  $chmod
	 * @param mixed  $chown
	 * @param mixed  $chgrp
	 * @return bool
	 */
	public function mkdir($path, $chmod = false, $chown = false, $chgrp = false ) {
		$path = untrailingslashit($path);
		if ( empty($path) )
			return false;

		if ( ! $this->ftp->mkdir($path) )
			return false;
		if ( ! $chmod )
			$chmod = FS_CHMOD_DIR;
		$this->chmod($path, $chmod);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		return true;
	}

	/**
<<<<<<< HEAD
	 * Deletes a directory.
	 *
	 * @since 2.5.0
	 *
	 * @param string $path      Path to directory.
	 * @param bool   $recursive Optional. Whether to recursively remove files/directories.
	 *                          Default false.
	 * @return bool True on success, false on failure.
	 */
	public function rmdir( $path, $recursive = false ) {
		return $this->delete( $path, $recursive );
	}

	/**
	 * Gets details for files in a directory or a specific file.
	 *
	 * @since 2.5.0
	 *
	 * @param string $path           Path to directory or file.
	 * @param bool   $include_hidden Optional. Whether to include details of hidden ("." prefixed) files.
	 *                               Default true.
	 * @param bool   $recursive      Optional. Whether to recursively include file details in nested directories.
	 *                               Default false.
	 * @return array|false {
	 *     Array of files. False if unable to list directory contents.
	 *
	 *     @type string $name        Name of the file or directory.
	 *     @type string $perms       *nix representation of permissions.
	 *     @type int    $permsn      Octal representation of permissions.
	 *     @type string $owner       Owner name or ID.
	 *     @type int    $size        Size of file in bytes.
	 *     @type int    $lastmodunix Last modified unix timestamp.
	 *     @type mixed  $lastmod     Last modified month (3 letter) and day (without leading 0).
	 *     @type int    $time        Last modified time.
	 *     @type string $type        Type of resource. 'f' for file, 'd' for directory.
	 *     @type mixed  $files       If a directory and $recursive is true, contains another array of files.
	 * }
	 */
	public function dirlist( $path = '.', $include_hidden = true, $recursive = false ) {
		if ( $this->is_file( $path ) ) {
			$limit_file = basename( $path );
			$path       = dirname( $path ) . '/';
=======
	 *
	 * @param string $path
	 * @param bool $recursive
	 * @return bool
	 */
	public function rmdir($path, $recursive = false ) {
		return $this->delete($path, $recursive);
	}

	/**
	 *
	 * @param string $path
	 * @param bool   $include_hidden
	 * @param bool   $recursive
	 * @return bool|array
	 */
	public function dirlist($path = '.', $include_hidden = true, $recursive = false ) {
		if ( $this->is_file($path) ) {
			$limit_file = basename($path);
			$path = dirname($path) . '/';
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		} else {
			$limit_file = false;
		}

		mbstring_binary_safe_encoding();

<<<<<<< HEAD
		$list = $this->ftp->dirlist( $path );
=======
		$list = $this->ftp->dirlist($path);
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
		if ( empty( $list ) && ! $this->exists( $path ) ) {

			reset_mbstring_encoding();

			return false;
		}

		$ret = array();
		foreach ( $list as $struc ) {

<<<<<<< HEAD
			if ( '.' == $struc['name'] || '..' == $struc['name'] ) {
				continue;
			}

			if ( ! $include_hidden && '.' == $struc['name'][0] ) {
				continue;
			}

			if ( $limit_file && $struc['name'] != $limit_file ) {
				continue;
			}

			if ( 'd' == $struc['type'] ) {
				if ( $recursive ) {
					$struc['files'] = $this->dirlist( $path . '/' . $struc['name'], $include_hidden, $recursive );
				} else {
					$struc['files'] = array();
				}
			}

			// Replace symlinks formatted as "source -> target" with just the source name
			if ( $struc['islink'] ) {
				$struc['name'] = preg_replace( '/(\s*->\s*.*)$/', '', $struc['name'] );
			}
=======
			if ( '.' == $struc['name'] || '..' == $struc['name'] )
				continue;

			if ( ! $include_hidden && '.' == $struc['name'][0] )
				continue;

			if ( $limit_file && $struc['name'] != $limit_file )
				continue;

			if ( 'd' == $struc['type'] ) {
				if ( $recursive )
					$struc['files'] = $this->dirlist($path . '/' . $struc['name'], $include_hidden, $recursive);
				else
					$struc['files'] = array();
			}

			// Replace symlinks formatted as "source -> target" with just the source name
			if ( $struc['islink'] )
				$struc['name'] = preg_replace( '/(\s*->\s*.*)$/', '', $struc['name'] );
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e

			// Add the Octal representation of the file permissions
			$struc['permsn'] = $this->getnumchmodfromh( $struc['perms'] );

			$ret[ $struc['name'] ] = $struc;
		}

		reset_mbstring_encoding();

		return $ret;
	}

	/**
<<<<<<< HEAD
	 * Destructor.
	 *
	 * @since 2.5.0
=======
>>>>>>> 7d7b7c61bccc41a65a5ab66598a91e99be57ce8e
	 */
	public function __destruct() {
		$this->ftp->quit();
	}
}
